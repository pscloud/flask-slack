broker_url = 'redis://localhost'
result_backend = 'rpc://'

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'Africa/Johannesburg'
enable_utc = True
