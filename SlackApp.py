from celery import Celery
from flask import Flask
from flask import request
from flask import json
import os
import random
import time
from flask import Flask, request, render_template, session, flash, redirect, \
    url_for, jsonify
from flask_mail import Mail, Message
import subprocess
import sys
# sys.path.append('libs/delegatorpy')
import delegator

slackdepl = Flask(__name__)
# slackdepl.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
# slackdepl.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
slackdepl.config.from_object('flaskconfig')

celery = Celery(slackdepl.name)
celery.config_from_object('celeryconfig')
celery.conf.update(slackdepl.config)

@slackdepl.route("/testdep/",methods=['POST'])
def testdep():
    content = request.get_json()
    print content
    if content['type'] == 'url_verification':
        return content['challenge']
    return "OK"

@slackdepl.route("/deploy",methods=['POST','GET'])
def maindeploy():
        content = request.get_json()
        print content
        subprocess.Popen(['debops-tasks','-a','fstrim -av','all','-i','inventory/hosts'],cwd='/shared/tracs-infra-new/ansible',stderr=PIPE,stdout=PIPE)
        return "EMPTY"

@slackdepl.route("/outhook",methods=['POST'])
def mainout():
    content = request.get_json()
    print content
    return '{ "text": "Welcome outhook from tracsvpn01!"}'

#if __name__ == "__main__":
print "testing"
#    slackdepl.run(host='0.0.0.0',debug=True)


@celery.task(bind=True)
def testdeploy(self):
    """Background task to execute ansible/debops"""
    for i in range(100):
        self.update_state(state='PROGRESS',meta={'current':i,'status':'doing: {0}...'.format(i)})
        time.sleep(1)
    return {'status': 'Done', 'result': 321}

@celery.task(bind=True)
def firstexec(self,env):
    """Test for pinging"""
    os.chdir('/shared/tracs-infra/ansible')
    c=delegator.run(['debops-tasks','-m ping','postgesql','-l',env])
    return {'status':'Done','result':c.out}

@slackdepl.route("/ping/<env>",methods=['GET'])
def ping(env):
    """Test for pinging"""
    os.chdir('/shared/tracs-infra/ansible')
    c=delegator.run(['debops-tasks','-m ping','postgesql','-l',env])
    return {'status':'Done','result':c.out}

@slackdepl.route("/execute", methods=['POST','GET'])
def execute():
    task = testdeploy.apply_async()
    return jsonify({'url':url_for('execstate', task_id=task.id)}), 202, {'Location': url_for('execstate', task_id=task.id)}

@slackdepl.route('/execstatus/<task_id>')
def execstate(task_id):
    task = testdeploy.AsyncResult(task_id)
    return jsonify({'state':task.state,
                    'current':task.info.get('current',0),
                    'status':task.info.get('status',1)
                    })
