https://stackoverflow.com/questions/21559572/celery-task-subprocess-stdout-to-log#28195298

```
child = subprocess.Popen(popenargs, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE, **kwargs)

log_level = {child.stdout: stdout_log_level,
             child.stderr: stderr_log_level}

def check_io():
    ready_to_read = select.select([child.stdout, child.stderr], [], [], 1000)[0]
    for io in ready_to_read:
        line = io.readline()
        logger.log(log_level[io], line[:-1])

# keep checking stdout/stderr until the child exits
while child.poll() is None:
    check_io()

check_io()  # check again to catch anything after the process exits
```



# Define variables like normal cli arguments ./playbook.yml -i hosts --var1=value --var2=value2  
# Run localhost playbooks with an empty inventory ./localhost_playbook.yml --var1=value  
# Pass other ansible-playbook options after a double-dash ./playbook.yml -i hosts -- --tags myapp

git clone https://github.com/bigpandaio/ansible-exec
cd ansible-exec
make install



https://stackoverflow.com/questions/16371552/stream-results-in-celery
